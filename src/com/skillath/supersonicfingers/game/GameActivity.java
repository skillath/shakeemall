package com.skillath.supersonicfingers.game;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.skillath.supersonicfingers.R;
import com.skillath.supersonicfingers.runtime.MainGamePanel;
import com.skillath.supersonicfingers.utils.GAME;
import com.skillath.supersonicfingers.utils.GameHelper;
import com.skillath.supersonicfingers.utils.GameOverDialog;
import com.skillath.supersonicfingers.utils.SQLiteHelper;

public class GameActivity extends Activity
{
	private RelativeLayout mRelativeLayout;
	private TextView mChronometerTextView;
	public TextView mTapsTextView;
	private TextView mStartTime;
	private MainGamePanel mMainGamePanel;
	private InterstitialAd mAd;
	private ProgressDialog mProgress = null;

	private RelativeLayout mChronoLayout;

	private Typeface mTypeFace;
	private CountDownTimer mStartTimer, mTimer;
	private Handler mHandler;
	private static SQLiteHelper mDB;

	private static GameHelper mGameHelper;

	public int mShowingPoints = 0;
	public int mCurrensSecTap = 0;
	private int mMaxTaps = 0;
	private int mPoints = 0;

	private GoogleApiClient mClient;

	public static void setDatabase(SQLiteHelper db)
	{
		mDB = db;
	}

	public static void setGameHelper(GameHelper helper)
	{
		mGameHelper = helper;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		if (mGameHelper != null)
			if (GAME.DEBUG)
				mGameHelper.enableDebugLog(true);

		if (GAME.internetConnectionEnabled(this))
		{
			if (GAME.canEnableAd())
			{
				mAd = new InterstitialAd(this);
				mAd.setAdUnitId(GAME.MY_AD_UNIT_ID);

				AdRequest adRequest = new AdRequest.Builder().addTestDevice("050C966DC6511AA568BE99A0CE280EC8").build();
				mAd.loadAd(adRequest);
				if (!mAd.isLoaded())
					mProgress = ProgressDialog.show(this, null, null, true, false);

				mAd.setAdListener(new AdListener()
				{

					@Override
					public void onAdClosed()
					{
						super.onAdClosed();
						init();
					}

					@Override
					public void onAdLoaded()
					{
						if (mProgress != null)
							mProgress.dismiss();
						mAd.show();
					}

					@Override
					public void onAdFailedToLoad(int errorCode)
					{
						if (mProgress != null)
							mProgress.dismiss();
						Toast.makeText(GameActivity.this, "Failed to load Ad. Check your internet connection.", Toast.LENGTH_LONG).show();
						super.onAdFailedToLoad(errorCode);
						init();
					}
				});
			}
			else
			{
				init();
			}
		}
		else
		{
			Toast.makeText(GameActivity.this, "Failed to load Ad. Check your internet connection.", Toast.LENGTH_LONG).show();
			init();
		}

		GAME.debug("GameActivity.onCreate();");
	}

	/**
	 * Called when the Ad disappears. Shows the game screen.
	 */
	private void init()
	{
		setContentView(R.layout.activity_game);
		if (mGameHelper != null)
		{
			mClient = mGameHelper.getApiClient();
			if (GAME.internetConnectionEnabled(this) && !mClient.isConnected())
				mClient.connect();
		}

		mTypeFace = Typeface.createFromAsset(getAssets(), GAME.TYPEFACE_EIGHT_BIT);

		mRelativeLayout = (RelativeLayout) findViewById(R.id.game_layout);
		mChronometerTextView = (TextView) findViewById(R.id.lbl_chronometer);
		mTapsTextView = (TextView) findViewById(R.id.lbl_taps);

		mChronoLayout = new RelativeLayout(this);
		mMainGamePanel = new MainGamePanel(this);
		mStartTime = new TextView(this);
		mHandler = new Handler();

		mChronoLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		mChronoLayout.setBackgroundColor(Color.parseColor("#774D4D4D"));
		mChronoLayout.setClickable(true);
		mChronoLayout.setGravity(Gravity.CENTER);

		mStartTime.setTextSize(75);
		mStartTime.setTextColor(Color.WHITE);

		mTapsTextView.setTypeface(mTypeFace);
		mChronometerTextView.setTypeface(mTypeFace);
		mStartTime.setTypeface(mTypeFace);

		mChronoLayout.addView(mStartTime);
		mRelativeLayout.addView(mMainGamePanel, 0);
		mRelativeLayout.addView(mChronoLayout);
		installTimers();
	}

	/**
	 * Installs all CountDownTimers for the gameplay.
	 */
	private void installTimers()
	{
		mStartTimer = new CountDownTimer(4000, 250)
		{

			@Override
			public void onTick(long millisUntilFinished)
			{
				if (millisUntilFinished / GAME.SECOND > 0)
					mStartTime.setText(String.valueOf(millisUntilFinished / GAME.SECOND));
				else
					mStartTime.setText("TAP!");
			}

			@Override
			public void onFinish()
			{
				mRelativeLayout.removeView(mChronoLayout);
				mTimer.start();
			}
		};
		mStartTimer.start();

		mTimer = new CountDownTimer(GAME.MAX_TIME, 250)
		{
			int time = 0;

			@Override
			public void onTick(long millisUntilFinished)
			{

				time += GAME.SECOND / 4;
				if (time == GAME.SECOND)
				{
					if (mCurrensSecTap > mMaxTaps)
						mMaxTaps = mCurrensSecTap;
					time = 0;
					mCurrensSecTap = 0;
				}
				if (millisUntilFinished / GAME.SECOND > 0)
					mChronometerTextView.setText(String.valueOf(millisUntilFinished / GAME.SECOND));
				else
					mChronometerTextView.setText("0");
			}

			@Override
			public void onFinish()
			{
				mChronometerTextView.setText("Time's up!");
				mMainGamePanel.setSurfaceClickable(false);
				updateDatabase();
				mHandler.postDelayed(new Runnable()
				{
					@Override
					public void run()
					{
						GameOverDialog.setDatabase(mDB);
						Dialog dialog = new GameOverDialog(GameActivity.this, R.style.MyDialog);
						dialog.setCancelable(false);
						dialog.show();
					}
				}, GAME.SECOND);
			}
		};
	}

	/**
	 * Updates the database and the Google Play Games Service
	 */
	private void updateDatabase()
	{
		if (mGameHelper != null)
		{
			GoogleApiClient mClient = mGameHelper.getApiClient();
			if (!mClient.isConnected())
				mClient.connect();
			GAME.debug("******************* GOOGLE_API_CLIENT: " + mClient.isConnected());
		}

		if (mPoints > Integer.parseInt(mDB.getMaxTaps()))
			mDB.updateMaxTaps(mPoints);

		if (mMaxTaps > Long.parseLong(mDB.getMaxSpeed()))
			mDB.updateMaxSpeed(mMaxTaps);

		mDB.updateRemaining(mPoints);

		if (mGameHelper != null)
		{
			if (mClient.isConnected() && GAME.internetConnectionEnabled(this))
			{
				long rem = Long.parseLong(mDB.getRemaining());
				int games = Integer.parseInt(mDB.getGames());

				Games.Leaderboards.submitScore(mClient, GAME.LB_TAPS, mPoints);
				Games.Leaderboards.submitScore(mClient, GAME.LB_VELOCITY, mMaxTaps);
				Games.Leaderboards.submitScore(mClient, GAME.LB_GLOBAL, (games * GAME.MAX_SCORE) + rem);
			}
		}
	}

	@Override
	protected void onPause()
	{
		if (mMainGamePanel != null)
		{
			mMainGamePanel.stop();
			mMainGamePanel = null;
			mRelativeLayout.removeAllViews();
		}
		super.onPause();
		if (mGameHelper != null)
			mGameHelper.onStop();
	}

	@Override
	protected void onStop()
	{
		if (mMainGamePanel != null)
		{
			mMainGamePanel.stop();
			mMainGamePanel = null;
			mRelativeLayout.removeAllViews();
		}
		super.onStop();
//		if (mGameHelper != null)
//			mGameHelper.onStop();
	}

	@Override
	protected void onDestroy()
	{
		if (mMainGamePanel != null)
		{
			mMainGamePanel.stop();
			mMainGamePanel = null;
			mRelativeLayout.removeAllViews();
		}
		super.onDestroy();
//		if (mGameHelper != null)
//			mGameHelper.onStop();
	}

	@Override
	protected void onStart()
	{
		super.onStart();
		if (mGameHelper != null)
			mGameHelper.onStart(this);
	}

	@Override
	public void onBackPressed()
	{
	}

	/**
	 * Called when "Retry" button is hit on the GameOverDialog
	 */
	public void reload()
	{
		Intent intent = getIntent();
		overridePendingTransition(0, 0);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		finish();
		overridePendingTransition(0, 0);
		startActivity(intent);
	}

	/**
	 * 
	 * @return mMaxTaps: MaxSpeed
	 */
	public int getTapSpeed()
	{
		return mMaxTaps;
	}

	/**
	 * 
	 * @param points
	 *            - int
	 */
	public void setPoints(int points)
	{
		mPoints = points;
	}

	/**
	 * 
	 * @return mPoints: Current game points
	 */
	public int getPoints()
	{
		return mPoints;
	}
}
