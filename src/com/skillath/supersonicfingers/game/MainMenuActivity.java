package com.skillath.supersonicfingers.game;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.games.Games;
import com.skillath.supersonicfingers.R;
import com.skillath.supersonicfingers.runtime.MainMenuPanel;
import com.skillath.supersonicfingers.utils.GAME;
import com.skillath.supersonicfingers.utils.GameHelper;
import com.skillath.supersonicfingers.utils.GameHelper.GameHelperListener;
import com.skillath.supersonicfingers.utils.SQLiteHelper;

public class MainMenuActivity extends Activity implements OnClickListener
{
	private SQLiteHelper mDBHelper = new SQLiteHelper(this, "Scores", null, 1);
	private TextView mStart;
	private TextView mTapsRemaing;
	private TextView mScores;
	private TextView mAchievements;
	private TextView mMaxSpeed;
	private TextView mMaxTaps;
	private RelativeLayout mRelative;
	private Typeface mTypeFace;
	private MainMenuPanel mMainMenuPanel;
	private Bundle mBundle;
	public GameHelper mGameHelper;
	private boolean mIsFirsTime = true;

	private Handler mHandler;

	final int RC_RESOLVE = 5000, RC_UNUSED = 5001;

	@SuppressLint("HandlerLeak")
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if (mIsFirsTime)
			startActivity(new Intent(this, SplashScreenActivity.class));
		setContentView(R.layout.activity_main_menu);

		mBundle = savedInstanceState;

		mTypeFace = Typeface.createFromAsset(getAssets(), GAME.TYPEFACE_EIGHT_BIT);

		mStart = (TextView) findViewById(R.id.start);
		mScores = (TextView) findViewById(R.id.scores);
		mAchievements = (TextView) findViewById(R.id.achievements);
		mTapsRemaing = (TextView) findViewById(R.id.lbl_taps_remaining);
		mMaxTaps = (TextView) findViewById(R.id.lbl_max_taps);
		mMaxSpeed = (TextView) findViewById(R.id.lbl_max_speed);
		mRelative = (RelativeLayout) findViewById(R.id.main_menu_layout);

		if (mHandler == null)
		{
			mHandler = new Handler()
			{
				@Override
				public void handleMessage(Message msg)
				{
					switch (msg.what)
					{
						case 0:
							mScores.setVisibility(View.VISIBLE);
							Toast.makeText(getApplicationContext(), "Connected to Google Play Games Services!", Toast.LENGTH_SHORT).show();
							break;
						case 1:
							mScores.setVisibility(View.GONE);
							break;
					}
				}
			};
		}

		mMainMenuPanel = new MainMenuPanel(this);

		mRelative.addView(mMainMenuPanel, 0);

		mStart.setTypeface(mTypeFace);
		mScores.setTypeface(mTypeFace);
		mTapsRemaing.setTypeface(mTypeFace);
		mMaxTaps.setTypeface(mTypeFace);
		mMaxSpeed.setTypeface(mTypeFace);
		mAchievements.setTypeface(mTypeFace);

		mAchievements.setVisibility(View.GONE);

		mMaxTaps.setText("Max Taps: " + mDBHelper.getMaxTaps());
		mMaxSpeed.setText("Max Speed: " + mDBHelper.getMaxSpeed() + "tap/s");

		mTapsRemaing.setText(mDBHelper.getRemaining());

		if ((GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS || GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SIGN_IN_REQUIRED) && GAME.internetConnectionEnabled(this))
		{
			if (mIsFirsTime)
			{
				if (mGameHelper == null)
					showConnectionDialog();
				else
				{
//					if (!mGameHelper.getApiClient().isConnected())
//						mGameHelper.getApiClient().connect();
					isGameHelperConnected();
				}
			}
			else
			{
				if (mGameHelper != null)
					isGameHelperConnected();
			}
		}
		else
		{
			Toast.makeText(this, "Google Play Services are disabled or not installed or must be updated.", Toast.LENGTH_SHORT).show();
			mScores.setVisibility(View.GONE);
		}

		mScores.setOnClickListener(this);
		mStart.setOnClickListener(this);
		mAchievements.setOnClickListener(this);

		mIsFirsTime = false;

		GAME.debug("MainMenuActivity.onCreate();");
	}

	private void showConnectionDialog()
	{
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle("Connect to Google Play Games");
		dialog.setMessage("Do you want to connect to Google Play Games service to compare your best scores with your friends? You need to sign in with Google+.");
		dialog.setCancelable(false);
		dialog.setPositiveButton("OK", new DialogInterface.OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
				connectGooglePlayGames();
			}
		});
		dialog.setNegativeButton("No, thanks", new DialogInterface.OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
				mScores.setVisibility(View.GONE);
			}
		});
		dialog.show();
	}

	private void connectGooglePlayGames()
	{
		mGameHelper = new GameHelper(this, GameHelper.CLIENT_ALL);

		GameHelperListener listener = new GameHelperListener()
		{

			@Override
			public void onSignInSucceeded()
			{
				isGameHelperConnected();
			}

			@Override
			public void onSignInFailed()
			{
				Toast.makeText(getApplicationContext(), "Sign In with Google Play Games Services failed.", Toast.LENGTH_SHORT).show();
				mScores.setVisibility(View.GONE);
				mGameHelper = null;
			}
		};

		mGameHelper.setup(listener);
		if (!mGameHelper.isSignedIn())
			mGameHelper.beginUserInitiatedSignIn();
		if (GAME.DEBUG)
			mGameHelper.enableDebugLog(true);
	}

	private void isGameHelperConnected()
	{
		if (mGameHelper != null)
		{
			if (!mGameHelper.getApiClient().isConnected())
			{
				mGameHelper.getApiClient().connect();
				new Thread(new Runnable()
				{

					@Override
					public void run()
					{
						boolean flag = true;
						while (flag)
						{
							if (mGameHelper.getApiClient().isConnected())
							{
								Message m = mHandler.obtainMessage();
								m.what = 0;
								mHandler.sendEmptyMessage(m.what);
								flag = false;
							}
							else
							{
								Message m = mHandler.obtainMessage();
								m.what = 1;
								mHandler.sendEmptyMessage(m.what);
							}
						}
					}
				}).start();
			}
			else
			{
				mScores.setVisibility(View.VISIBLE);
			}
		}
	}

	@Override
	protected void onStart()
	{
		super.onStart();
		if (mGameHelper != null)
			mGameHelper.onStart(this);
	}

	@Override
	protected void onResume()
	{
		this.onCreate(mBundle);
		if (mGameHelper != null)
			isGameHelperConnected();
	}

	@Override
	protected void onPause()
	{
		if (mMainMenuPanel != null)
		{
			mMainMenuPanel.stop();
			mMainMenuPanel = null;
		}
		super.onPause();
	}

	@Override
	protected void onStop()
	{
		if (mMainMenuPanel != null)
		{
			mMainMenuPanel.stop();
			mMainMenuPanel = null;
		}
		super.onStop();
		if (mGameHelper != null)
			mGameHelper.onStop();
	}

	@Override
	protected void onDestroy()
	{
		if (mMainMenuPanel != null)
		{
			mMainMenuPanel.stop();
			mMainMenuPanel = null;
		}
		super.onDestroy();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		mGameHelper.onActivityResult(requestCode, resultCode, data);
		this.onCreate(mBundle);
		isGameHelperConnected();
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.scores:
				if (mGameHelper != null)
					if (mGameHelper.getApiClient().isConnected())
						startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(mGameHelper.getApiClient()), RC_UNUSED);
				break;
			case R.id.start:
				if (mGameHelper != null)
				{
					if (mGameHelper.isConnecting() || mGameHelper.getApiClient().isConnecting())
					{
						Toast.makeText(getApplicationContext(), "Connecting to Google Play Games Services. Please, wait a few moments.", Toast.LENGTH_LONG).show();
						break;
					}
					GameActivity.setGameHelper(mGameHelper);
				}
				GameActivity.setDatabase(mDBHelper);
				startActivity(new Intent(this, GameActivity.class));
				break;
			case R.id.achievements:
				startActivityForResult(Games.Achievements.getAchievementsIntent(mGameHelper.getApiClient()), RC_UNUSED);
				break;
		}
	}

	public int getGames()
	{
		return Integer.parseInt(mDBHelper.getGames());
	}

	public long getPoints()
	{
		return Long.parseLong(mDBHelper.getRemaining());
	}
}
