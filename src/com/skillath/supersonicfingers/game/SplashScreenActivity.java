package com.skillath.supersonicfingers.game;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.skillath.supersonicfingers.R;
import com.skillath.supersonicfingers.utils.GAME;

public class SplashScreenActivity extends Activity
{

	private ProgressBar mProgressBar;
	private TextView mTitle, mLoading;
	private Typeface mTypeFace;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);

		mTypeFace = Typeface.createFromAsset(getAssets(), GAME.TYPEFACE_EIGHT_BIT);

		mProgressBar = (ProgressBar) findViewById(R.id.pr_loading);
		mTitle = (TextView) findViewById(R.id.txt_title);
		mLoading = (TextView) findViewById(R.id.txt_loading);

		mTitle.setTypeface(mTypeFace);
		mLoading.setTypeface(mTypeFace);

		new CountDownTimer(5000, 1000)
		{

			@Override
			public void onTick(long millisUntilFinished)
			{
				mProgressBar.setProgress(mProgressBar.getProgress() + 1);
			}

			@Override
			public void onFinish()
			{
				mProgressBar.setProgress(mProgressBar.getProgress() + 1);
				SplashScreenActivity.this.finish();
			}
		}.start();
	}
}
