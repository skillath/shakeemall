package com.skillath.supersonicfingers.game;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

import com.skillath.supersonicfingers.R;
import com.skillath.supersonicfingers.utils.GAME;

public class AboutActivity extends Activity
{
	private Typeface mTypeFace;
	private TextView mAbout;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);

		mTypeFace = Typeface.createFromAsset(getAssets(), GAME.TYPEFACE_EIGHT_BIT);

		mAbout = (TextView) findViewById(R.id.about_activity);
		mAbout.setTypeface(mTypeFace);

		mAbout.setText(getResources().getString(R.string.developer_name));

		GAME.debug("AboutActivity.onCreate();");
	}

}
