package com.skillath.supersonicfingers.game;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

import com.skillath.supersonicfingers.R;
import com.skillath.supersonicfingers.utils.GAME;

public class ScoresActivity extends Activity
{

	private Typeface mTypeFace;
	private TextView mScores;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scores);
		
		mTypeFace = Typeface.createFromAsset(getAssets(), GAME.TYPEFACE_EIGHT_BIT);
		
		mScores = (TextView) findViewById(R.id.scores_activity);
		mScores.setTypeface(mTypeFace);
		
		GAME.debug("ScoresActivity.onCreate();");
	}
}
