package com.skillath.supersonicfingers.runtime;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class MainGameThread extends Thread
{
	private boolean mRunning = false;
	private SurfaceHolder mSurfaceHolder;
	private MainGamePanel mGamePanel;

	private final static int MAX_FPS = 50;
	private final static int MAX_FRAME_SKIPS = 15;
	private final static int FRAME_PERIOD = 1000 / MAX_FPS;

	public void setRunning(boolean running)
	{
		mRunning = running;
	}

	public MainGameThread(SurfaceHolder surfaceHolder, MainGamePanel gamePanel)
	{
		super();
		mSurfaceHolder = surfaceHolder;
		mGamePanel = gamePanel;
	}

	@Override
	public void run()
	{
		Canvas canvas;
		long beginTime; // the time when the cycle begun
		long timeDiff; // the time it took for the cycle to execute
		int sleepTime; // ms to sleep (<0 if we're behind)
		int framesSkipped; // number of frames being skipped 

		sleepTime = 0;

		while (mRunning)
		{
			canvas = null;
			// try locking the canvas for exclusive pixel editing
			// in the surface
			try
			{
				canvas = this.mSurfaceHolder.lockCanvas();
				synchronized (mSurfaceHolder)
				{
					beginTime = System.currentTimeMillis();
					framesSkipped = 0; // resetting the frames skipped
					// update game state 
					mGamePanel.update();
					// render state to the screen
					// draws the canvas on the panel
					mGamePanel.render(canvas);
					// calculate how long did the cycle take
					timeDiff = System.currentTimeMillis() - beginTime;
					// calculate sleep time
					sleepTime = (int) (FRAME_PERIOD - timeDiff);

					if (sleepTime > 0)
					{
						try
						{
							Thread.sleep(sleepTime);
						}
						catch (InterruptedException e)
						{
						}
					}

					while (sleepTime < 0 && framesSkipped < MAX_FRAME_SKIPS)
					{
						// we need to catch up
						this.mGamePanel.update(); // update without rendering
						sleepTime += FRAME_PERIOD; // add frame period to check if in next frame
						framesSkipped++;
					}

					// for statistics
					// calling the routine to store the gathered statistics
				}
			}
			finally
			{
				// in case of an exception the surface is not left in 
				// an inconsistent state
				if (canvas != null)
				{
					mSurfaceHolder.unlockCanvasAndPost(canvas);
				}
			} // end finally
		}
	}
}
