package com.skillath.supersonicfingers.runtime;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.skillath.supersonicfingers.R;
import com.skillath.supersonicfingers.game.GameActivity;
import com.skillath.supersonicfingers.models.Tap;
import com.skillath.supersonicfingers.utils.GAME;

public class MainGamePanel extends SurfaceView implements SurfaceHolder.Callback
{
	private MainGameThread mThread;
	private GameActivity mContext;
	private Tap mTap;
	private Bitmap mEnergy;
	private boolean mIsClickable = true;
	private ArrayList<Tap> mTaps = new ArrayList<Tap>();

	public MainGamePanel(Context context)
	{
		super(context);
		getHolder().addCallback(this);
		mContext = (GameActivity) context;
		mEnergy = BitmapFactory.decodeResource(getResources(), R.drawable.energy_sprite);

		mThread = new MainGameThread(getHolder(), this);
		setFocusable(true);
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
	{
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder)
	{

		mThread.setRunning(true);
		mThread.start();
		GAME.debug(getClass().getSimpleName() + ".surfaceCreated();");
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder)
	{
		boolean retry = true;
		while (retry)
		{
			try
			{
				mThread.setRunning(false);
				mThread.join();
				retry = false;
			}
			catch (InterruptedException e)
			{
			}
		}
		GAME.debug(getClass().getSimpleName() + ".surfaceDestroyed();");
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if (mIsClickable)
		{
			if (event.getAction() == MotionEvent.ACTION_DOWN)
			{
				mTap = new Tap(getContext(), mEnergy, (int) event.getRawX(), (int) event.getRawY(), getWidth(), getHeight());
				mTaps.add(mTap);
				mContext.mCurrensSecTap++;
				int points = mContext.getPoints();
				points++;
				mContext.setPoints(points);
			}
		}

		return true;
	}

	public void setSurfaceClickable(boolean clickable)
	{
		mIsClickable = clickable;
	}

	public void render(Canvas canvas)
	{
		canvas.drawColor(Color.BLACK);
		if (!mTaps.isEmpty())
		{
			for (int i = 0; i < mTaps.size(); i++)
			{
				mTap = mTaps.get(i);
				mTap.draw(canvas);
			}
		}
	}

	public void update()
	{
		for (int i = 0; i < mTaps.size(); i++)
		{
			mTap = mTaps.get(i);
			if (mTap != null && mTap.isAlive())
				mTap.update(getHolder().getSurfaceFrame());
		}
	}

	public void stop()
	{
		mTaps.clear();
		mTap = null;
		mEnergy.recycle();
		mEnergy = null;
		surfaceDestroyed(getHolder());
	}

}
