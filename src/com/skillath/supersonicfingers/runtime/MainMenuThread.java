package com.skillath.supersonicfingers.runtime;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class MainMenuThread extends Thread
{

	private SurfaceHolder mSurfaceHolder;
	private MainMenuPanel mMenuPanel;
	private boolean mRunning;

	private final static int MAX_FPS = 50;
	private final static int MAX_FRAME_SKIPS = 15;
	private final static int FRAME_PERIOD = 1000 / MAX_FPS;

	public void setRunning(boolean running)
	{
		mRunning = running;
	}
	
	public boolean isRunning()
	{
		return mRunning;
	}

	public MainMenuThread(SurfaceHolder surfaceHolder, MainMenuPanel menuPanel)
	{
		super();
		mSurfaceHolder = surfaceHolder;
		mMenuPanel = menuPanel;
	}

	@Override
	public void run()
	{
		Canvas canvas;
		long beginTime; // the time when the cycle begun
		long timeDiff; // the time it took for the cycle to execute
		int sleepTime; // ms to sleep (<0 if we're behind)
		int framesSkipped; // number of frames being skipped 
		while (mRunning)
		{
			canvas = null;
			try
			{
				canvas = mSurfaceHolder.lockCanvas();
				synchronized (mSurfaceHolder)
				{
					beginTime = System.currentTimeMillis();
					framesSkipped = 0;
					mMenuPanel.update();
					mMenuPanel.render(canvas);
					timeDiff = System.currentTimeMillis() - beginTime;
					sleepTime = (int) (FRAME_PERIOD - timeDiff);

					if (sleepTime > 0)
					{
						try
						{
							Thread.sleep(sleepTime);
						}
						catch (InterruptedException e)
						{
						}
					}

					while (sleepTime < 0 && framesSkipped < MAX_FRAME_SKIPS)
					{
						// we need to catch up
						this.mMenuPanel.update(); // update without rendering
						sleepTime += FRAME_PERIOD; // add frame period to check if in next frame
						framesSkipped++;
					}
				}
			}
			finally
			{
				if (canvas != null)
				{
					mSurfaceHolder.unlockCanvasAndPost(canvas);
				}
			}
		}
	}
}
