package com.skillath.supersonicfingers.runtime;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.skillath.supersonicfingers.R;
import com.skillath.supersonicfingers.game.MainMenuActivity;
import com.skillath.supersonicfingers.models.EnergyParticle;
import com.skillath.supersonicfingers.models.Particle;
import com.skillath.supersonicfingers.utils.GAME;

public class MainMenuPanel extends SurfaceView implements SurfaceHolder.Callback
{

	private MainMenuThread mThread;
	private MainMenuActivity mContext;
	private Particle mParticle;
	private EnergyParticle mEnergyParticle;
	private ArrayList<Particle> mParticles = new ArrayList<Particle>();
	private ArrayList<EnergyParticle> mEnergyParticles = new ArrayList<EnergyParticle>();

	public MainMenuPanel(Context context)
	{
		super(context);
		getHolder().addCallback(this);
		mContext = (MainMenuActivity) context;

		if (mThread == null)
			mThread = new MainMenuThread(getHolder(), this);
		setFocusable(false);
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
	{
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder)
	{
		if (mThread != null && !mThread.isRunning())
		{
			mThread.setRunning(true);
			mThread.start();
			init();
			GAME.debug(getClass().getSimpleName() + ".surfaceCreated();");
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder)
	{
		boolean retry = true;
		while (retry)
		{
			try
			{
				if (mThread != null)
				{
					mThread.setRunning(false);
					mThread.join();
					mThread = null;
				}
				retry = false;

			}
			catch (InterruptedException e)
			{
			}
		}
		GAME.debug(getClass().getSimpleName() + ".surfaceDestroyed();");
	}

	private void init()
	{
		for (long i = 0; i < mContext.getPoints() / 1000; i++)
		{
			mParticle = new Particle(Particle.rndInt(0, getWidth()), Particle.rndInt(0, getHeight()));
			mParticles.add(mParticle);
		}

		for (int i = 0; i < mContext.getGames(); i++)
		{
			if (mEnergyParticles.size() < 10)
			{
				mEnergyParticle = new EnergyParticle(BitmapFactory.decodeResource(getResources(), R.drawable.energy_sprite), EnergyParticle.rndInt(0, getWidth()), EnergyParticle.rndInt(0, getWidth()));
				mEnergyParticles.add(mEnergyParticle);
			}
		}
	}

	public void stop()
	{
		mEnergyParticles.clear();
		mEnergyParticle = null;
		mParticles.clear();
		mParticle = null;
		surfaceDestroyed(getHolder());
	}

	public void update()
	{
		for (int i = 0; i < mParticles.size(); i++)
		{
			mParticle = mParticles.get(i);
			if (mParticle != null)
				mParticle.update(getHolder().getSurfaceFrame());
		}
		for (int i = 0; i < mEnergyParticles.size(); i++)
		{
			mEnergyParticle = mEnergyParticles.get(i);
			if (mEnergyParticle != null)
				mEnergyParticle.update(getHolder().getSurfaceFrame());
		}
	}

	public void render(Canvas canvas)
	{
		canvas.drawColor(Color.BLACK);
		if (!mParticles.isEmpty())
		{
			for (int i = 0; i < mParticles.size(); i++)
			{
				mParticle = mParticles.get(i);
				mParticle.draw(canvas);
			}
		}
		if (!mEnergyParticles.isEmpty())
		{
			for (int i = 0; i < mEnergyParticles.size(); i++)
			{
				mEnergyParticle = mEnergyParticles.get(i);
				mEnergyParticle.draw(canvas);
			}
		}
	}

}
