package com.skillath.supersonicfingers.utils;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.skillath.supersonicfingers.R;
import com.skillath.supersonicfingers.game.GameActivity;

public class GameOverDialog extends Dialog implements android.view.View.OnClickListener
{
	private static SQLiteHelper mDB;
	private GameActivity mContext;
	private Button mRetry, mExit;
	private TextView mScore, mBestScore, mRemaining, mSpeed, mBestSpeed;
	private Typeface mTypeface;
	private AdView mAdView;

	public GameOverDialog(Context context)
	{
		super(context);
		mContext = (GameActivity) context;
	}

	public GameOverDialog(Context context, int theme)
	{
		super(context, theme);
		mContext = (GameActivity) context;
	}

	public static void setDatabase(SQLiteHelper db)
	{
		mDB = db;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.game_over_dialog);

		mTypeface = Typeface.createFromAsset(mContext.getAssets(), GAME.TYPEFACE_EIGHT_BIT);

		mRetry = (Button) findViewById(R.id.god_btn_retry);
		mExit = (Button) findViewById(R.id.god_btn_exit);
		mScore = (TextView) findViewById(R.id.god_score);
		mBestScore = (TextView) findViewById(R.id.god_best_score);
		mRemaining = (TextView) findViewById(R.id.god_remaining);
		mSpeed = (TextView) findViewById(R.id.god_speed);
		mBestSpeed = (TextView) findViewById(R.id.god_best_speed);
		mAdView = (AdView) findViewById(R.id.adView);

		if (GAME.internetConnectionEnabled(mContext.getApplicationContext()))
		{
			AdRequest adRequest = new AdRequest.Builder().build();
			mAdView.loadAd(adRequest);
		}
		else
			mAdView.setVisibility(View.GONE);

		String maxSpeed = mDB.getMaxSpeed();
		String maxTaps = mDB.getMaxTaps();
		String remaining = mDB.getRemaining();

		if (mContext.getTapSpeed() >= Integer.parseInt(maxSpeed))
			mSpeed.setTextColor(Color.GREEN);
		else
			mSpeed.setTextColor(Color.RED);

		if (mContext.getPoints() >= Integer.parseInt(maxTaps))
			mScore.setTextColor(Color.GREEN);
		else
			mScore.setTextColor(Color.RED);

		mSpeed.setTypeface(mTypeface);
		mSpeed.setText("Velocity: " + mContext.getTapSpeed() + "taps/s");

		mBestSpeed.setTypeface(mTypeface);
		mBestSpeed.setText("Best velocity: " + maxSpeed + "taps/s");

		mScore.setTypeface(mTypeface);
		mScore.setText("Score: " + mContext.getPoints());

		mBestScore.setTypeface(mTypeface);
		mBestScore.setText("Best score: " + maxTaps);

		mRemaining.setTypeface(mTypeface);
		mRemaining.setTextSize(mRemaining.getTextSize() + 2);
		mRemaining.setText("TAPS DONE\n\n" + remaining + "\n\n");

		GAME.debug("TAPS DONE\n" + remaining);

		mRetry.setTypeface(mTypeface);
		mExit.setTypeface(mTypeface);

		mRetry.setOnClickListener(this);
		mExit.setOnClickListener(this);
	}

	@SuppressLint("NewApi")
	@Override
	public void onClick(View v)
	{
		this.dismiss();
		switch (v.getId())
		{
			case R.id.god_btn_exit:
				mContext.finish();
				break;
			case R.id.god_btn_retry:
				mContext.reload();
				break;
		}
	}
}
