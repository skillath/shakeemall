package com.skillath.supersonicfingers.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class GAME
{
	public static final String PACKAGE = "com.skillath.supersonicfingers";
	public static final String TAG = "GAME";
	public static final String TYPEFACE_EIGHT_BIT = "fonts/8bits.ttf";
	public static final long MAX_SCORE = Long.valueOf("1000000");
	public static final int MAX_TIME = 10000;
	public static final int ADS_COUNT = 3;
	public static final int SECOND = 1000;

	public static final String MY_AD_UNIT_ID = "ca-app-pub-2851904291133996/9206547134";
	public static final String MY_AD_BANNER_UNIT_ID = "ca-app-pub-2851904291133996/3505410737";

	public static final String LB_GLOBAL = "CgkI7Zqhrb8REAIQCw";
	public static final String LB_VELOCITY = "CgkI7Zqhrb8REAIQBg";
	public static final String LB_TAPS = "CgkI7Zqhrb8REAIQBw";
	public static final String AC_FIRST_GAME = "CgkI7Zqhrb8REAIQAA";
	public static final String AC_1000 = "CgkI7Zqhrb8REAIQAg";
	public static final String AC_100_IN_1 = "CgkI7Zqhrb8REAIQAQ";
	public static final String AC_COMPLETED = "CgkI7Zqhrb8REAIQAw";
	public static final String AC_THAT_FINGERS = "CgkI7Zqhrb8REAIQBA";

	private static int adCounts = 2;

	public static boolean canEnableAd()
	{
		adCounts++;
		if (adCounts == ADS_COUNT)
		{
			adCounts = 0;
			return true;
		}
		return false;
	}

	public static final boolean DEBUG = false;

	public static void debug(String message)
	{
		if (DEBUG)
			Log.d(TAG, message);
	}

	public static boolean internetConnectionEnabled(Context contexto)
	{
		ConnectivityManager conMgr = (ConnectivityManager) contexto.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo i = conMgr.getActiveNetworkInfo();
		if (i == null)
			return false;
		if (!i.isConnected())
			return false;
		if (!i.isAvailable())
			return false;
		return true;
	}

}
