package com.skillath.supersonicfingers.utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHelper extends SQLiteOpenHelper
{

	String mCreateTable = "CREATE TABLE SCORES(id INTEGER, taps INTEGER, speed REAL, remaining INTEGER, games INTEGER, PRIMARY KEY(id))";
	String mInsertFirstTime = "INSERT INTO SCORES(id, taps, speed, remaining, games) VALUES(" + 1 + "," + 0 + ", " + 0 + ", " + 0 + ", " + 0 + ")";

	public SQLiteHelper(Context context, String name, CursorFactory factory, int version)
	{
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		db.execSQL(mCreateTable);
		db.execSQL(mInsertFirstTime);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		db.execSQL("DROP TABLE IF EXISTS SCORES");
		db.execSQL(mCreateTable);
	}

	public String getMaxTaps()
	{
		String bestScores = "";

		String selectQuery = "SELECT * FROM SCORES";

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst())
		{
			do
			{
				bestScores = cursor.getString(1);
			} while (cursor.moveToNext());
		}

		cursor.close();

		return bestScores;
	}

	public void updateRemaining(int taps)
	{
		String update = "";

		if (Long.parseLong(getRemaining()) + taps >= GAME.MAX_SCORE)
		{
			update = "UPDATE SCORES SET remaining = " + 0 + " WHERE id = " + 1;
			updateGames();
		}
		else
			update = "UPDATE SCORES SET remaining = " + (Long.parseLong(getRemaining()) + taps) + " WHERE id = " + 1;

		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL(update);
	}

	public void updateMaxTaps(int maxTaps)
	{
		String update = "UPDATE SCORES SET taps = " + maxTaps + " WHERE id = " + 1;
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL(update);
	}

	public void updateMaxSpeed(float maxSpeed)
	{
		String update = "UPDATE SCORES SET speed = " + maxSpeed + " WHERE id = " + 1;
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL(update);
	}

	public void updateGames()
	{
		int games = Integer.parseInt(getGames()) + 1;
		String update = "UPDATE SCORES SET speed = " + games + " WHERE id = " + 1;
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL(update);
	}

	public String getMaxSpeed()
	{
		String maxSpeed = "";
		String selectQuery = "SELECT * FROM SCORES";

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst())
		{
			do
			{
				maxSpeed = cursor.getString(2);
			} while (cursor.moveToNext());
		}

		cursor.close();

		return maxSpeed;
	}

	public String getGames()
	{
		String games = "";
		String selectQuery = "SELECT * FROM SCORES";

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst())
		{
			do
			{
				games = cursor.getString(4);
			} while (cursor.moveToNext());
		}

		cursor.close();

		return games;
	}

	public String getRemaining()
	{
		String remaining = "";
		String selectQuery = "SELECT * FROM SCORES";

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst())
		{
			do
			{
				remaining = cursor.getString(3);
			} while (cursor.moveToNext());
		}

		cursor.close();

		return remaining;
	}

}
