package com.skillath.supersonicfingers.models;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public class Particle
{
	public static final int MAX_DIMENSION = 5; // the maximum width or height
	public static final int MAX_SPEED = 15; // maximum speed (per update)

	private int state; // particle is alive or dead
	private float widht; // width of the particle
	private float height; // height of the particle
	private float x, y; // horizontal and vertical position
	private double xv, yv; // vertical and horizontal velocity
	private int color; // the color of the particle
	private Paint paint; // internal use to avoid instantiation

	public int getState()
	{
		return state;
	}

	public void setState(int state)
	{
		this.state = state;
	}

	public Particle(int x, int y)
	{
		this.x = x;
		this.y = y;
		this.widht = MAX_DIMENSION;
		this.height = this.widht;
		//		this.height = rnd(1, MAX_DIMENSION);
		this.xv = (rndDbl(1, MAX_SPEED * 2) - MAX_SPEED);
		this.yv = (rndDbl(1, MAX_SPEED * 2) - MAX_SPEED);
		// smoothing out the diagonal speed
		if (xv * xv + yv * yv > MAX_SPEED * MAX_SPEED)
		{
			xv *= 0.7;
			yv *= 0.7;
		}
		this.color = Color.argb(255, rndInt(0, 255), rndInt(0, 255), rndInt(0, 255));
		this.paint = new Paint(this.color);
	}

	/**
	 * Resets the particle
	 * 
	 * @param x
	 * @param y
	 */
	public void reset(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	// Return an integer that ranges from min inclusive to max inclusive.
	public static int rndInt(int min, int max)
	{
		return (int) (min + Math.random() * (max - min + 1));
	}

	static double rndDbl(double min, double max)
	{
		return min + (max - min) * Math.random();
	}

	public void update(Rect container)
	{
		if (this.x <= container.left || this.x >= container.right - this.widht)
		{
			this.xv *= -1;
		}
		// Bottom is 480 and top is 0 !!!
		if (this.y <= container.top || this.y >= container.bottom - this.height)
		{
			this.yv *= -1;
		}
		this.x += this.xv;
		this.y += this.yv;
	}

	public void draw(Canvas canvas)
	{
		//		paint.setARGB(255, 128, 255, 50);
		paint.setColor(this.color);
		canvas.drawRect(this.x, this.y, this.x + this.widht, this.y + this.height, paint);
		//		canvas.drawCircle(x, y, widht, paint);
	}
}
