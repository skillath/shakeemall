package com.skillath.supersonicfingers.models;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public class EnergyParticle
{
	public static final int MAX_DIMENSION = 5; // the maximum width or height
	public static final int MAX_SPEED = 5; // maximum speed (per update)

	int mX, mY;

	private float widht; // width of the particle
	private float height; // height of the particle
	private double xv, yv; // vertical and horizontal velocity
	private int color; // the color of the particle
	private Paint paint; // internal use to avoid instantiation
	private Bitmap mBitmap;

	public EnergyParticle(Bitmap bitmap, int x, int y)
	{
		mBitmap = bitmap;
		mX = x;
		mY = y;
		this.widht = MAX_DIMENSION;
		this.height = this.widht;
		//		this.height = rnd(1, MAX_DIMENSION);
		this.xv = (rndDbl(1, MAX_SPEED * 2) - MAX_SPEED);
		this.yv = (rndDbl(1, MAX_SPEED * 2) - MAX_SPEED);
		// smoothing out the diagonal speed
		if (xv * xv + yv * yv > MAX_SPEED * MAX_SPEED)
		{
			xv *= 0.7;
			yv *= 0.7;
		}
		this.color = Color.argb(255, rndInt(0, 255), rndInt(0, 255), rndInt(0, 255));
		this.paint = new Paint(this.color);
	}

	public void reset(int x, int y)
	{
		mX = x;
		mY = y;
	}

	// Return an integer that ranges from min inclusive to max inclusive.
	public static int rndInt(int min, int max)
	{
		return (int) (min + Math.random() * (max - min + 1));
	}

	static double rndDbl(double min, double max)
	{
		return min + (max - min) * Math.random();
	}

	public void update(Rect container)
	{
		// update with collision
		if (mX <= container.left || mX >= container.right - this.widht)
		{
			this.xv *= -1;
		}
		// Bottom is 480 and top is 0 !!!
		if (mY <= container.top || mY >= container.bottom - this.height)
		{
			this.yv *= -1;
		}
		mX += this.xv;
		mY += this.yv;
	}

	public void draw(Canvas canvas)
	{
		canvas.drawBitmap(mBitmap, mX - mBitmap.getWidth() / 2, mY - mBitmap.getHeight() / 2, paint);
	}

}
