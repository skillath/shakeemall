package com.skillath.supersonicfingers.models;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import com.skillath.supersonicfingers.game.GameActivity;

public class Tap
{
	public static final int STATE_ALIVE = 0; // particle is alive
	public static final int STATE_DEAD = 1; // particle is dead

	public static final int DEFAULT_LIFETIME = 200; // play with this
	public static final int MAX_DIMENSION = 5; // the maximum width or height
	public static final int MAX_Y_SPEED = 10; // maximum speed (per update)
	public static final int MAX_X_SPEED = 2; // maximum speed (per update)

	int mX, mY;
	int mPoints = 0;

	private GameActivity mContext;
	private int mState; // particle is alive or dead
	private float mWidth; // width of the particle
	//private float mHeight; // height of the particle
	private double mXV, mYV; // vertical and horizontal velocity
	private int mAge; // current age of the particle
	private int mLifetime; // particle dies when it reaches this value
	private int mColor; // the color of the particle
	private Paint mPaint; // internal use to avoid instantiation
	private Bitmap mBitmap;

	public int getState()
	{
		return mState;
	}

	public void setState(int state)
	{
		mState = state;
	}

	// helper methods -------------------------
	public boolean isAlive()
	{
		return mState == STATE_ALIVE;
	}

	public boolean isDead()
	{
		return mState == STATE_DEAD;
	}

	public Tap(Context context, Bitmap bitmap, int x, int y, int width, int height)
	{
		mContext = (GameActivity) context;
		mBitmap = bitmap;
		mX = x;
		mY = y;
		//paint = new Paint();

		mState = STATE_ALIVE;
		mWidth = rndInt(1, MAX_DIMENSION);
		//Height = mWidth;
		//		height = rnd(1, MAX_DIMENSION);
		mLifetime = DEFAULT_LIFETIME;
		mAge = 0;

		if (mX > mWidth / 2)
			mXV = -MAX_X_SPEED;
		else if (mX < mWidth / 2)
			mXV = MAX_X_SPEED;
		else
			mXV = 0;

		mYV = -MAX_Y_SPEED;

		mColor = Color.argb(255, rndInt(0, 255), rndInt(0, 255), rndInt(0, 255));
		mPaint = new Paint(mColor);
	}

	public void reset(int x, int y)
	{
		mState = STATE_ALIVE;
		mX = x;
		mY = y;
		mAge = 0;
	}

	// Return an integer that ranges from min inclusive to max inclusive.
	static int rndInt(int min, int max)
	{
		return (int) (min + Math.random() * (max - min + 1));
	}

	static double rndDbl(double min, double max)
	{
		return min + (max - min) * Math.random();
	}

	public void update()
	{
		if (mState != STATE_DEAD)
		{
			mX += mXV;
			mY += mYV;

			// extract alpha
			int a = mColor >>> 24;
			a -= 2; // fade by 5
			if (a <= 0)
			{ // if reached transparency kill the particle
				mState = STATE_DEAD;
			}
			else
			{
				mColor = (mColor & 0x00ffffff) + (a << 24); // set the new alpha
				mPaint.setAlpha(a);
				mAge++; // increase the age of the particle
//				mWidth *= 1.05;
				mYV -= 0.25f;
				//mHeight *= 1.05;
			}
			if (mAge >= mLifetime)
			{ // reached the end if its life
				mState = STATE_DEAD;
			}
		}
	}

	public void update(Rect container)
	{
		// update with collision
		if (isAlive())
		{
			if (mX == container.right / 2)
				mXV = 0;
			else if (mX > container.right / 2)
				mXV = -MAX_X_SPEED;
			else if (mX < container.right / 2)
				mXV = MAX_X_SPEED;

			if (mY <= container.top)
			{
				mState = STATE_DEAD;
				mContext.runOnUiThread(new Runnable()
				{

					@Override
					public void run()
					{
						mContext.mShowingPoints++;
						mContext.mTapsTextView.setText(String.valueOf(mContext.mShowingPoints));
					}
				});
			}
		}
		update();
	}

	public void draw(Canvas canvas)
	{
		if (isAlive())
			canvas.drawBitmap(mBitmap, mX - mBitmap.getWidth() / 2, mY - mBitmap.getHeight() / 2, mPaint);
	}

}
